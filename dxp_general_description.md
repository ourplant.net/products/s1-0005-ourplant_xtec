The modular design of the OurPlant XTec guarantees adaptation to individual and rapidly changing process requirements.
Up to five integrable processing heads, give a maximum of process variety and flexibility to this machine. OurPlant XTec can be equipped with various modules. Diverse applications in the field of micro assembly and micro dispensing can be combined with each other.

**Advantages:**
– Ideal for small and medium batch sizes
– Low set-up effort thanks to real plug & play capability
– Easy integration into a production line by using a transport system
– Software based system control

**Technical information**
– 2-axis gantry system (X, Y)
– Processing heads with integrated Z-axis
– Control panel
– Software and control with integrated industrial PC
– UV protection glass

**Note**: The system is equipped with UV protection glass as standard and is not suitable for laser soldering applications. For a laser configuration, please select the OurPlant XTec laser with UV and laser protection glass.