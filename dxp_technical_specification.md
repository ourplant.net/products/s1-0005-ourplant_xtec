| Parameter                                                    | Value |
| :----------------------------------------------------------- | ----- |
| **General:**                                                 |       |
| Dimensions in mm (W x D x H)                                 |       |
| Weight in kg                                                 | 1.200  |
| Compressed air range in bar (+specifications if applicable)  |  ≥ 5 bar (optimal > 6 bar), 100 l/min, gereinigt, trocken, ölfrei  |
| Voltage in V/DC (V/AC)                                       |       |
| Max. current in A                                            |       |
| Communication interface                                      |       |
| Functional temperature range in °C                           |  20 … 30     |
| Max. relative humidity in %                                  |  60   |
| Electronic connectors (Stecker - Ende des Kabels was vom Produkt abgeht) |       |
| Pneumatic connectors (Stecker - Ende des Kabels was vom Produkt abgeht) |       |
| **Movements:**                                               |       |
| Movement range in Z in mm                                    |       |
| Accuracy z-axis in mm                                        |       |
| Max. Speed Z-axis in mm/s                                    |       |
| Axis resolution Z-axis in µm                                 |       |
| Z movement (component height) in mm                          |       |
| Movement range rotation axis /A-axis in degrees              |       |
| Resolution rotation axis in arcsec                           |       |
| Movement range of swivel axis (flip/rotation angle) in degrees |       |
| Movement range Hexapod in mm (X-, Y-, Z-axis)                |       |
| Movement range Hexapod (Θx-, Θy-, Θz-axis) in degrees        |       |
| Movement range of the needle system in mm (X, Y)             |       |
| Stroke of the needle system in mm                            |       |
| Stroke in mm                                                 |       |
| Measuring path in mm                                         |       |
| Opening path in mm                                           |       |
| **Camera:**                                                  |       |
| Lens type / Type of objective                                |       |
| Field of view in mm (W x H)                                  |       |
| Camera resolution in mm/(µm)                                 |       |
| **Touch probe:**                                             |       |
| Probe accuracy in µm                                         |       |
| Probe repeatability in µm                                    |       |
| Max. permissible shear force in N                            |       |
| **Direct Dispensing:**                                       |       |
| Adjustment accuracy in mm                                    |       |
| Adjustment range in mm                                       |       |
| **Dispensing:**                                              |       |
| Cartridge holder/size                                        |       |
| Min. dispensing volume in µl                                 |       |
| Min. diameter metering point in mm                           |       |
| Max. Temperature needle heating in °C                        |       |
| Capacity of drip tray in ml                                  |       |
| **Laser:**                                                   |       |
| Emission spectrum in nm                                      |       |
| Wavelength in nm                                             |       |
| Min. diameter laser spot in mm                               |       |
| Working distance in mm                                       |       |
| Laser type                                                   |       |
| Pyrometer properties                                         |       |
| **Substrate sizes / capacities:**                            |       |
| Max. substrate size in inch / mm (B x T)                     |       |
| Min. substrate height / thickness in mm                      |       |
| Max. Magazingröße in Zoll / mm (B x T)                       |       |
| Anzahl der Magazine                                          |       |
| Max. wafer size in inch / mm                                 |       |
| Capacity Waffel Pack Adapter (EWW)                           |       |
| Capacty magazine (EWW)                                       |       |
| Substratbreite in mm                                         |       |
| **Other:**                                                   |       |
| Max. Stacking height in mm                                   |       |
| Force measuring range in N                                   |       |
| Force in N                                                   |       |
| Max. moving load in kg                                       |       |
| Permissible product weight on the Hexapod in kg              |       |
| Max. Weight for Z-stroke in kg                               |       |
| Max. Load capacity in kg                                     |       |
| Max. Temperature tool in °C                                  |       |
| Max. Temperature in °C                                       |       |
| Power in W                                                   |       |
| Lighting                                                     |       |
| Time per flip                                                |       |
| Theoretical force at 6 bar, forward flow                     |       |
| **Machine:**                                                 |       |
| Set-up area / cencter-to-center distance of the machine feet in mm (W x D) |  1.135 x 1.105     |
| Außenabmessungen in mm (W x D x H)                           |  1.735 x 1.455 x 2.400  |
| Max. functional area in mm (X,Y)                             | 500 x 500  |
| Repeatability in mm                                          | ± 0,001 |
| Max. load X-axis in kg                                       |       |
| Max. load Y-axis in kg                                       |       |
| Max. speed X-axis in mm/s                                    | 1.600    |
| Max. speed Y-axis in mm/s                                    | 1.600    |
| Max. acceleration X-axis in mm/s²                            |  25.000  |
| Max. acceleration Y-axis in mm/s²                            |  15.000  |
| Power supply                                                 |  400 VAC, 3L + N + PE 50 … 60 Hz, 16 A  |
| Operating Voltage                                            |       |
| Energy requirements in kW                                    |  0,7     |
| Certification                                                |  CE konform, ESD Schutz, Laser Schutz     |
| Min. load capacity table in kg                               |       |
| Transport dimensions in mm (W x D x H)                       |       |
| Dimensions of the machine feet in mm (diameter/B x T)     |  120   |
| Minimum distance to surrounding objects (front and back) in mm |       |