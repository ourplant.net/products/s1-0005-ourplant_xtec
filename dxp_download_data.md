Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s1-0005-ourplant_xtec).

| document | download options |
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s1-0005-ourplant_xtec/-/raw/main/01_operating_manual/S1-0005_F_BA_OurPlant%20XTec.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s1-0005-ourplant_xtec/-/raw/main/02_assembly_drawing/s1-0005-D_ZNB_ourplant_xtec.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s1-0005-ourplant_xtec/-/raw/main/03_circuit_diagram/S1_0005_0006_R_EPLAN_OurPlant_XTec-_Laser.pdf)|
|maintenance instructions   |[de](https://gitlab.com/ourplant.net/products/s1-0005-ourplant_xtec/-/raw/main/04_maintenance_instructions/S1-0005_E_WA_OurPlant%20XTec.pdf)|
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s1-0005-ourplant_xtec/-/raw/main/05_spare_parts/S1-0005_D_EVL_OurPlant%20XTec.pdf), [en](https://gitlab.com/ourplant.net/products/s1-0005-ourplant_xtec/-/raw/main/05_spare_parts/S1-0005_D_SWP_OurPlant%20XTec.pdf)|
