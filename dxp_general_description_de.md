Fertigen Sie innerhalb kürzester Zeit Ihre komplexen Produkte mit einem kompakten Vollautomaten in Serie. Die OurPlant XTec ist die Allround-Lösung für die herausfordernden Aufgaben der Mikromontage. Das modulare Baukastensystem der OurPlant XTec garantiert die Anpassung an individuelle und sich schnell ändernde Prozessanforderungen.

Dank der hohen Variabilität von Bearbeitungsköpfen und Grundplattenmodulen ermöglicht die OurPlant XTec höchste Prozessvielfalt und -flexibilität. Unterschiedlichste Applikationen der Mikrobestückung, -dosierung sowie -laserlöten sind somit in Kombination abbildbar.

**Vorteile:**
– Ideal für kleine und mittlere Losgrößen
– Geringe Rüstaufwände durch echte Plug & Play–Fähigkeit
– Einfache Integration in eine Fertigungslinie durch Einsatz eines Transportsystems
– Softwarebasierte Systemsteuerung

**Technische Daten:**
– 2-Achsportalsystem (X, Y)
– Bearbeitungsköpfe mit integrierter Z-Achse
– Bedienerpanel und UV-Schutzscheiben, optional mit Laserschutzscheiben
– Software und Steuerung mit integriertem Industrie-PC
– Interface mit 10 elektrischen Anschlüssen (5x CAN, 5x Ethernet) für die Aufnahme von Bearbeitungsmodulen bis zu einer Gesamtbreite von 150 mm (in der Standardausführung), durch eine Adapterplatte optional erweiterbar bis 300 mm

Die Anlage ist standardmäßig mit UV-Schutzglas ausgerüstet und ist nicht für Applikationen im Bereich Laserlöten geeignet. Für eine Laserkonfiguration bitte die OurPlant XTec Laser mit UV- und Laserschutzglas auswählen.